const express = require('express');
const app = express();
const fs = require('fs').promises;
const morgan = require('morgan');
const path = require('path');
const filesDir = path.join(__dirname, '/files');

app.use(morgan('tiny'));
app.use(express.json());

app.post('/api/files', async (req, res, next) => {

  if (isValidPostData(req.body, res)) {

    try {
      await fs.mkdir(filesDir, {
        recursive: true
      }, (err) => {
        if (err) {
          throw err;
        }
      });

      next();

    } catch (err) {

      if (err.code !== 'EEXIST') {

        res.status(500).json({
          "message": "Server error"
        });
      }
    }
  } else {

    return;
  }
});

app.post('/api/files', async (req, res) => {

  const {
    filename,
    content
  } = req.body;

  try {
    await fs.writeFile(path.join(filesDir, filename), content, 'utf-8', (error) => {
      if (error) {
        throw error;
      }
    });

    res.status(200).json({
      "message": "File created successfully"
    });

  } catch (err) {
    res.status(500).json({
      "message": "Server error"
    });
  }
});

app.get('/api/files', async (req, res) => {

  try {
    const files = await fs.readdir(filesDir);

    const uploadedFilesList = files.map((file) => {
      if (isValidFilename(file)) {

        return file;
      }
    });

    res.status(200).json({
      "message": "Success",
      "files": uploadedFilesList
    });

  } catch (err) {
    res.status(500).json({
      "message": "Server error"
    });
  }
});

app.get('/api/files/:filename', (req, res, next) => {

  const filename = req.params.filename;

  fs.access(path.join(filesDir, filename))
    .then(() => next())
    .catch(() => res.status(400).json({
      "message": `No file with '${filename}' filename found`
    }));
});

app.get('/api/files/:filename', async (req, res, next) => {

  const filename = req.params.filename;
  const extension = path.extname(filename);

  try {
    const stats = await fs.stat(path.join(filesDir, filename));

    fs.readFile(path.join(filesDir, filename), 'utf8').then((content) => {

      res.status(200).json({
        "message": "Success",
        "filename": filename,
        "content": content,
        "extension": extension.split('.')[1],
        "uploadedDate": stats.birthtime
      });
    });

  } catch (err) {
    res.status(500).json({
      "message": "Server error"
    });
  }
});

app.put('/api/files/:filename', (req, res, next) => {

  const filename = req.params.filename;

  fs.access(path.join(filesDir, filename))
    .then(() => next())
    .catch(() => res.status(400).json({
      "message": `No file with '${filename}' filename found`
    }));
});

app.put('/api/files/:filename', async (req, res, next) => {

  const filename = req.params.filename;
  const {
    filename: newFilename,
    content: newContent
  } = req.body;

  try {
    fs.readFile(path.join(filesDir, filename), 'utf8')
      .then(async (content) => {
        if (newContent && content !== newContent) {

          try {
            await fs.writeFile(path.join(filesDir, filename), newContent, 'utf8');

            if (newFilename && filename !== newFilename && isValidFilename(newFilename)) {

              next();
            } else {
              res.status(200).json({
                "message": `${filename} has been updated`
              });
            }

          } catch (error) {
            res.status(500).json({
              "message": "Server error"
            });
          }

        } else if (newFilename && filename === newFilename &&
          newContent && content === newContent) {
          res.status(200).json({
            "message": `${filename} is already up to date`
          });
        } else if (newFilename && filename !== newFilename 
          && isValidFilename(newFilename)) {
    
          next();
        }

      });
  } catch (error) {
    res.status(500).json({
      "message": "Server error"
    });
  }
});

app.put('/api/files/:filename', (req, res) => {

  const filename = req.params.filename;
  const {
    filename: newFilename
  } = req.body;

  try {
    fs.rename(path.join(filesDir, filename), path.join(filesDir, newFilename));

    res.status(200).json({
      "message": `${filename} has been updated`
    });
  } catch(error) {
    res.status(500).json({
      "message": "Server error"
    });
  }
});

app.delete('/api/files/:filename', (req, res) => {
  const filename = req.params.filename;

  fs.unlink(path.join(filesDir, filename))
    .then(() => {
      res.status(200).json({
        "message": `${filename} has been deleted`
      });
    })
    .catch(() => {
      res.status(400).json({
        "message": `No file with ${filename} filename has been found`
      });
    });
});

function isValidPostData(data, res) {

  if (data.filename) {
    if (isValidFilename(data.filename, res)) {
      if (data.content) {

        return true;
      } else {
        res.status(400).json({
          "message": `Please specify 'content' parameter`
        });

        return false;
      }
    }

  } else {
    res.status(400).json({
      "message": `Please specify 'filename' parameter`
    });
    return false;
  }
}

function isValidFilename(filename, res) {
  const regexp = /(?:^.+)\.(txt|js|log|json|yaml|xml)$/gm;

  if (regexp.test(filename)) {

    return true;
  } else {

    res.status(400).json({
      "message": `Wrong file extension`
    });

    return false;
  }
}

app.listen(8080, () => {
  console.log('Server started on port 8080');
});
